#!/bin/bash

# Get the logs of the users on the system
logs=$(who)

# Extract reference lines from the website
references=$(curl -s https://www.ac.upc.edu/ca/nosaltres/serveis-tic/blog | grep "Actualització"| sed -e 's/<[^>]*>//g')

# Print the logs followed by the references
echo "$logs"
echo "References:"
echo "$references"
