#!/bin/bash

# Retrieve the latest kernel version from kernel.org
latest_kernel=$(curl -s https://www.kernel.org/ | grep -A 1 "mainline:" | awk -F '<td><strong>|</strong>' '{printf $2}')

# Print the latest kernel version
echo "The latest mainline kernel version is $latest_kernel"

# Write the latest kernel version to a file named last.kernel
echo "$latest_kernel" > last.kernel # en el examen el fitxer es guarda a /var/lib/last.kernel