#!/bin/bash

# Get the number of users connected to the system
connected_users=$(who | wc -l)

# Get the number of disabled accounts
disabled_accounts=$(grep '^[^:]*:[^!*]' /etc/passwd | wc -l)

# Print the results
echo "Number of connected users: $connected_users"
echo "Number of disabled accounts: $disabled_accounts"
